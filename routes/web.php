<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|

| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [Controller::class, 'index']);

// ===============
// ===============

Route::get('/home', function () {
    return view('home', [
        "title" => "home"
    ]);
});

Route::get('/about', function()
    {
        return view('about', [
            "title" => "about",
            "name" => "Amiruddin Aziz",
            "email" => "aamiruddinaziz06@yahoo.co.id",
            "image" => "aku.png"
        ]);
    }
);

Route::get('/blog', function()
    {
        return view('blog',[
            "title" => "blog"
        ]);
    }
);

Route::get('/Verivication', function()
    {
        return view('Verivication');
    }
);