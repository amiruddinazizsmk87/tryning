@extends('layouts.main')

@section('container')
    <h1>Ini halaman About</h1>
    <h3>{{ $name }}</h3>
    {{-- <h3>Sekolah</h3> --}}
    <p>{{ $email }}</p>
    <img src="img/{{ $image }}" alt="{{ $name }}" width="300">
@endsection